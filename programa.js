// Crear un objeto  (variables compuestas) para representar el valor


//L representa a la biblioteca leaflet


let miMapa = L.map('mapid');

//definir caracteristcas del visor 

miMapa.setView([4.710989,-74.072090], 16);
let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
miProveedor.addTo(miMapa);

//crear un objeto marcador 
var miMarcador = L.marker([4.748203,-74.102084]);

miMarcador.addTo(miMapa)

//Json
let circle = L.circle([4.748203,-74.102084], {
    color: 'blue',
    fillColor: 'blue',
    fillOpacity: 0.5,
    radius: 60
});

circle.addTo(miMapa)